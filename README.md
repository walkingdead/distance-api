If you are on Linux distributive you can get ukpostcodesmysql.sql in following way

- chmod +x database.sh
- ./database.sh

If you are on Windows machine:

- Download archive `https://www.freemaptools.com/download/full-postcodes/ukpostcodesmssql.zip`
- Unzip it
- Copy ukpostcodesmysql.sql to src/main/resources/scripts


H2 console could be accessed on http://localhost:8090/console/
(enter dbc:h2:mem:testdb as db url)


To run unit and integration tests use command
    `./gradlew test`

How to start application.

Basically you have two options 

 1) Open your project inside IDE and launch com.distance.DistanceApiApplication and specify VM options
    parameter `-Dspring.profiles.active=dev`

 2) - execute command ./gradlew build
    - launch application with following command
    `java -Dspring.profiles.active=dev -jar /build/libs/distance-api-1.0-SNAPSHOT.jar`
    
 
 Before you start test API get Jwt token
 Hit URL
     POST http://localhost:8090/auth
     
     Payload:
     
     {"username":"admin@gmail.com", "password":"admin"}
 
 You'll get token in response. Put this token to header
     `Authorization` and after you'll have access to all API.

 API list
 
 1) Get Distance 
 
   `http://localhost:8090/distance?fromPostcode=AB10%201XG&toPostcode=AB21%207XA`
    
    Response:
     
    {
        "locations": [
            {
                "postcode": "AB10 1XG",
                "longitude": -2.114847768,
                "latitude": 57.14416516
            },
            {
                "postcode": "AB21 7XA",
                "longitude": -2.161656,
                "latitude": 57.225777
            }
        ],
        "distance": 10.45968615575176,
        "unit": "km"
    }
 
 2) Get post code by id
    
   `http://localhost:8090/postcode/1`
    
    Response:
    
    {
        "id": 1,
        "postcode": "AB10 1XG",
        "latitude": 57.14416516,
        "longitude": -2.114847768
    }
    
 3) Get post code by code
   
   `http://localhost:8090/postcode/code/AB21%209DY`
    
    Response:
   
    {
       "id": 116,
       "postcode": "AB21 9DY",
       "latitude": 57.18217946,
       "longitude": -2.177874891
    }
 
 4) Get post codes by page
 
   `http://localhost:8090/postcode?page=1&size=2`
    
    Response:
    
    {
        "content": [
            {
                "id": 1,
                "postcode": "AB10 1XG",
                "latitude": 57.14416516,
                "longitude": -2.114847768
            },
            {
                "id": 2,
                "postcode": "AB10 6RN",
                "latitude": 57.13787976,
                "longitude": -2.121486688
            }
        ],
        "totalPages": 870725,
        "totalElements": 1741450,
        "last": false,
        "numberOfElements": 2,
        "sort": null,
        "first": true,
        "size": 2,
        "number": 0
    }
   
  5) Update post code
    
    `http://localhost:8090/postcode`
    
    PUT
    
    Payload:
    
    {"id":128, "postcode":"AB21 9EQ", "latitude": 20, "longitude":-2.5}
    
  6) Create post code  
  
    `http://localhost:8090/postcode`
      
     POST
      
     Payload:
      
      {"id":null, "postcode":"AB11 6UM", "latitude": 70, "longitude":-12.5}
      
      Response:
      
      {
          "id": 1741533
      }
      
Notes:
    
    When application starts it loads all post codes database to memory. So it can take a while up to 1 minute. 
    It will consume up to 2 GB of RAM
    If you have issues with RAM you can fill credentials for MySQl in `src/main/resources/application-backup.yml`
    Also you have to apply all scripts from  `src/main/resources/scripts`
    Hope this will not happen :)