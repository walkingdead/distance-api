package com.distance.it.controller;

import com.distance.common.PostcodePageable;
import com.distance.common.SecurityAuthenticator;
import com.distance.common.annotation.FunctionalTestRunner;
import com.distance.dto.PostcodeDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import java.util.Map;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class PostcodeControllerTestIT {

    private static final String ADMIN_LOGIN = "admin@gmail.com";
    private static final String ADMIN_PASSWORD = "admin";

    private static final String USER_LOGIN = "user@gmail.com";
    private static final String USER_PASSWORD = "user";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private SecurityAuthenticator securityAuthenticator;

    @Value("${local.server.port}")
    private String port;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getPostCodeById() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<PostcodeDto> response = restTemplate.exchange(
                "http://localhost:{port}/postcode/{id}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port, 1);
        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        PostcodeDto postcodeDto = response.getBody();

        assertThat("Post code", postcodeDto.getPostcode(), is("AB10 1XG"));
        assertThat("Post code latitude", postcodeDto.getLatitude(), is(57.14416516));
    }

    @Test
    public void getPostCodeByIdNotFound() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, USER_LOGIN, USER_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("404 Not Found");

        restTemplate.exchange(
                "http://localhost:{port}/postcode/{id}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port, 10000000);
    }

    @Test
    public void getPostCodeByCode() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<PostcodeDto> response = restTemplate.exchange(
                "http://localhost:{port}/postcode/code/{code}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port, "AB15 5HB");
        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        PostcodeDto postcodeDto = response.getBody();

        assertThat("Post code", postcodeDto.getPostcode(), is("AB15 5HB"));
        assertThat("Post code latitude ", postcodeDto.getLatitude(), is(57.147428));
        assertThat("Post code longitude", postcodeDto.getLongitude(), is(-2.1472662));
    }

    @Test
    public void getPostCodeByCodeNotFound() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("404 Not Found");

        restTemplate.exchange(
                "http://localhost:{port}/postcode/code/{code}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port, "BB2 4EF");
    }

    @Test
    public void getPostCodeByIdUnAuthorized() throws Exception {

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("401 Unauthorized");

         restTemplate.exchange(
                "http://localhost:{port}/postcode/{id}",
                HttpMethod.GET, null, new ParameterizedTypeReference<PostcodeDto>() {
                }, port, 1);
    }

    @Test
    public void getPostCodes() throws Exception {

        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<PostcodePageable> response =  restTemplate.exchange(
                "http://localhost:{port}/postcode?page={page}&size={size}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodePageable>() {
                }, port, 1, 2);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        PostcodePageable postcodePageable = response.getBody();

        assertThat("Post code", postcodePageable.getContent().size(), is(2));
        PostcodeDto firstPostcode = postcodePageable.getContent().get(0);
        PostcodeDto secondPostcode = postcodePageable.getContent().get(1);

        assertThat("First post code", firstPostcode.getPostcode(), is("AB10 1XG"));
        assertThat("Post code longitude", firstPostcode.getLongitude(), is(-2.114847768));
        assertThat("Second post code", secondPostcode.getPostcode(), is("AB10 6RN"));
        assertThat("Second code latitude", secondPostcode.getLatitude(), is(57.13787976));
    }

    @Test
    public void getPostCodesInvalidPageParameters() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, USER_LOGIN, USER_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        restTemplate.exchange(
                "http://localhost:{port}/postcode?page={page}&size={size}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<PostcodePageable>() {
                }, port, -1, 2);
    }

    @Test
    public void createPostcodeForbidden() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, USER_LOGIN, USER_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(null, "BB2 4EF", 5D, -2D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("403 Forbidden");

        restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<PostcodePageable>() {
                }, port);
    }

    @Test
    public void updatePostcodeForbidden() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, USER_LOGIN, USER_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(1L, "BB2 4EF", 5D, -2D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("403 Forbidden");

        restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<PostcodePageable>() {
                }, port);
    }

    @Test
    public void createPostcode() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(null, "BC2 4EF", 12D, -2D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        ResponseEntity<Map<String, Long>> response = restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<Map<String, Long>>() {
                }, port);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));
        Map<String, Long> postcodeSavedId = response.getBody();
        assertThat("Post code id", postcodeSavedId.get("id"), notNullValue());
    }

    @Test
    public void createPostcodeInvalidPayload() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(null, "BC28UH4EF", 12D, -2D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.POST, authEntity, new ParameterizedTypeReference<Map<String, Long>>() {
                }, port);
    }

    @Test
    public void updatePostcode() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(494L, "BB1 4DZ", 18D, -8D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        ResponseEntity<PostcodeDto> response = restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port);

        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.NO_CONTENT));
    }

    @Test
    public void updatePostcodeInvalidPayload() throws Exception {
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        headers.setContentType(MediaType.APPLICATION_JSON);
        PostcodeDto postcodeDto = new PostcodeDto(null, "BB1 4DZ", 18D, -8D);
        HttpEntity<String> authEntity = new HttpEntity<>(objectMapper.writeValueAsString(postcodeDto), headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        ResponseEntity<PostcodeDto> response = restTemplate.exchange(
                "http://localhost:{port}/postcode",
                HttpMethod.PUT, authEntity, new ParameterizedTypeReference<PostcodeDto>() {
                }, port);
    }
}
