package com.distance.it.controller;

import com.distance.common.SecurityAuthenticator;
import com.distance.common.annotation.FunctionalTestRunner;
import com.distance.dto.DistanceDto;
import com.distance.dto.LocationDto;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import static java.util.stream.Collectors.toList;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@FunctionalTestRunner
public class DistanceControllerTestIT {

    private static final String ADMIN_LOGIN = "admin@gmail.com";
    private static final String ADMIN_PASSWORD = "admin";

    private static final String USER_LOGIN = "user@gmail.com";
    private static final String USER_PASSWORD = "user";

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private SecurityAuthenticator securityAuthenticator;

    @Value("${local.server.port}")
    private String port;


    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void getDistance() throws Exception{
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        ResponseEntity<DistanceDto> response = restTemplate.exchange(
                "http://localhost:{port}/distance?fromPostcode={fromPostcode}&toPostcode={toPostcode}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<DistanceDto>() {
                }, port, "AB10 1XG", "AB16 7NX");
        assertThat("Response status should be ok", response.getStatusCode(), equalTo(HttpStatus.OK));

        DistanceDto distanceDto = response.getBody();

        assertThat("Distance", distanceDto.getDistance(), is(5.8578528584027865));
        assertThat("Distance unit", distanceDto.getUnit(), is("km"));
        assertThat("", distanceDto.getLocations().stream().map(LocationDto::getPostcode).collect(toList()),
                containsInAnyOrder("AB10 1XG", "AB16 7NX"));
    }

    @Test
    public void getDistanceInvalidFromCode() throws Exception{
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, ADMIN_LOGIN, ADMIN_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        restTemplate.exchange(
                "http://localhost:{port}/distance?fromPostcode={fromPostcode}&toPostcode={toPostcode}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<DistanceDto>() {
                }, port, "AB10676761XG", "AB16 7NX");
    }

    @Test
    public void getDistanceInvalidToCode() throws Exception{
        HttpHeaders headers = securityAuthenticator.getAuthHeader(restTemplate, port, USER_LOGIN, USER_PASSWORD);
        HttpEntity<String> authEntity = new HttpEntity<>(headers);

        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("400 Bad Request");

        restTemplate.exchange(
                "http://localhost:{port}/distance?fromPostcode={fromPostcode}&toPostcode={toPostcode}",
                HttpMethod.GET, authEntity, new ParameterizedTypeReference<DistanceDto>() {
                }, port, "AB10 1XG", "AB10676761XG");
    }

    @Test
    public void getDistanceUnAuthorized() throws Exception{
        expectedException.expect(HttpClientErrorException.class);
        expectedException.expectMessage("401 Unauthorized");

        restTemplate.exchange(
                "http://localhost:{port}/distance?fromPostcode={fromPostcode}&toPostcode={toPostcode}",
                HttpMethod.GET, null, new ParameterizedTypeReference<DistanceDto>() {
                }, port, "AB10676761XG", "AB16 7NX");
    }
}
