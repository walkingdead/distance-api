package com.distance.it.service;

import com.distance.common.annotation.IntegrationTestRunner;
import com.distance.dto.PostcodeDto;
import com.distance.exceptions.PostcodeDuplicateException;
import com.distance.exceptions.PostcodeNotFoundException;
import com.distance.model.Postcode;
import com.distance.repository.PostcodeRepository;
import com.distance.service.PostcodeService;
import com.distance.utils.DistanceUtils;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.transaction.Transactional;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@IntegrationTestRunner
@RunWith(SpringJUnit4ClassRunner.class)
@Transactional
public class PostcodeServiceTestIT {

    @Autowired
    private PostcodeService postcodeService;

    @Autowired
    private PostcodeRepository postcodeRepository;

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    @WithMockUser(authorities = "USER")
    public void getPostcodeByIdTest(){
        Postcode postcode = postcodeService.getById(10L);
        assertThat("Postcode id", postcode.getId(), is(10L));
        assertThat("Postcode code", postcode.getPostcode(), is("AB12 9SP"));
        assertThat("Postcode latitude", postcode.getLatitude(), is(57.14870708));
        assertThat("Postcode longitude", postcode.getLongitude(), is(-2.097806027));
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void getPostcodeByIdNotFoundTest(){
        expectedException.expect(PostcodeNotFoundException.class);
        expectedException.expectMessage("Postcode id [10000] not found");
        postcodeService.getById(10000L);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void getPostcodeByCodeTest(){
        Postcode postcode = postcodeService.getByCode("AB21 0TF");
        assertThat("Postcode id", postcode.getId(), is(21L));
        assertThat("Postcode code", postcode.getPostcode(), is("AB21 0TF"));
        assertThat("Postcode latitude", postcode.getLatitude(), is(57.221883));
        assertThat("Postcode longitude", postcode.getLongitude(), is(-2.273318));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void getPostcodeByCodeNotFoundTest(){
        expectedException.expect(PostcodeNotFoundException.class);
        expectedException.expectMessage("Postcode [AB25 1UX] not found");
         postcodeService.getByCode("AB25 1UX");
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void getPostcodesTest(){
        Page<PostcodeDto> postcodes = postcodeService.getPostcodes(0, 2);
        assertThat("Postcode total elements", postcodes.getTotalElements(), is(499L));
        assertThat("Postcode size", postcodes.getContent().size(), is(2));

        PostcodeDto firstPostcode = postcodes.getContent().get(0);
        PostcodeDto secondPostcode = postcodes.getContent().get(1);

        assertThat("First postcode code", firstPostcode.getPostcode(), is("AB10 1XG"));
        assertThat("Second postcode code", secondPostcode.getPostcode(), is("AB10 6RN"));
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void updatePostcodesForbiddenForUserTest(){
        expectedException.expect(AccessDeniedException.class);
        expectedException.expectMessage("Access is denied");
        PostcodeDto postcodeDto = new PostcodeDto(1L, "AB10 1XG", 1D, 2D);
        postcodeService.updatePostcode(postcodeDto);
    }

    @Test
    @WithMockUser(authorities = "USER")
    public void createPostcodesForbiddenForUserTest(){
        expectedException.expect(AccessDeniedException.class);
        expectedException.expectMessage("Access is denied");
        PostcodeDto postcodeDto = new PostcodeDto(1L, "AB10 1XG", 1D, 2D);
        postcodeService.createPostcode(postcodeDto);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void createPostcodesTest(){
        PostcodeDto postcodeDto = new PostcodeDto(null, "BB2 4EF", 5D, -2D);
        Postcode postcode = postcodeService.createPostcode(postcodeDto);

        assertThat("Created postcode code id", postcode.getId(), notNullValue());
        assertThat("Created postcode code", postcode.getPostcode(), is("BB2 4EF"));
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void createPostcodesDuplicateTest(){
        expectedException.expect(PostcodeDuplicateException.class);
        expectedException.expectMessage("Postcode BB1 4EF already exists");
        PostcodeDto postcodeDto = new PostcodeDto(null, "BB1 4EF", 5D, -2D);
        postcodeService.createPostcode(postcodeDto);
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    public void updatePostcodesTest(){
        Postcode postcode = postcodeService.getByCode("BB1 4EF");
        postcode.setLatitude(10D);
        postcode.setLongitude(-1D);
        Postcode updatedPostcode = postcodeService.updatePostcode(DistanceUtils.toPostcodeDto(postcode));
        postcodeRepository.flush();


        assertThat("Updated postcode code latitude", updatedPostcode.getLatitude(), is(10.0));
        assertThat("Updated postcode code longitude", updatedPostcode.getLongitude(), is(-1.0));
    }
}
