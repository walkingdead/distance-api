package com.distance.unit.service;


import com.distance.model.Postcode;
import com.distance.service.DistanceService;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class DistanceServiceTest {


    private DistanceService distanceService = new DistanceService();

    @Test
    public void testCalculateDistance() {
        Postcode fromPostcodeData = new Postcode(1L, "AB10 1XG", 57.144165160000000, -2.114847768000000);
        Postcode toPostcodeData = new Postcode(16L, "AB15 9SE", 57.118647620000000, -2.174250607000000);
        assertEquals(7.1846896473517186, distanceService.calculateDistance(fromPostcodeData, toPostcodeData), 0);
    }

    @Test
    public void testCalculateDistanceSamePoints() {
        Postcode fromPostcodeData = new Postcode(1L, "AB10 1XG", 57.144165160000000, -2.114847768000000);
        Postcode toPostcodeData = new Postcode(1L, "AB10 1XG", 57.144165160000000, -2.114847768000000);
        assertEquals(0.0, distanceService.calculateDistance(fromPostcodeData, toPostcodeData), 0);
    }
}
