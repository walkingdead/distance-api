package com.distance.unit.controller;

import com.distance.controller.DistanceController;
import com.distance.exceptions.PostcodeNotFoundException;
import com.distance.model.Postcode;
import com.distance.service.DistanceService;
import com.distance.service.PostcodeService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(DistanceController.class)
public class DistanceApiControllerTest {

    private static final Long FROM_POSTCODE_ID = 1L;
    private static final String FROM_POSTCODE = "AB14 0TQ";
    private static final String FROM_PARAM_NAME = "fromPostcode";

    private static final Long TO_POSTCODE_ID = 2L;
    private static final String TO_POSTCODE = "AB15 6NA";
    private static final String TO_PARAM_NAME = "toPostcode";

    private static final String ROOT_PATH = "/distance";
    private static final String ADMIN = "admin@gmail.com";
    private static final String NON_VALID_POST_CODE = "AB156NA";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @MockBean
    private PostcodeService postcodeService;

    @MockBean
    private DistanceService distanceService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void getDistanceTest() throws Exception {
        Postcode firstPostcodeData = new Postcode(FROM_POSTCODE_ID, FROM_POSTCODE, 57.14870708, -2.097806027);
        Postcode secondPostcodeData = new Postcode(TO_POSTCODE_ID, TO_POSTCODE, 57.10155692, -2.2684857520);
        when(postcodeService.getByCode(FROM_POSTCODE)).thenReturn(firstPostcodeData);
        when(postcodeService.getByCode(TO_POSTCODE)).thenReturn(secondPostcodeData);
        when(distanceService.calculateDistance(firstPostcodeData, secondPostcodeData)).thenReturn(3.781216);

        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(FROM_PARAM_NAME, FROM_POSTCODE)
                        .param(TO_PARAM_NAME, TO_POSTCODE))
                .andExpect(status().isOk())
                .andExpect(content().json("{\"locations\":[{\"postcode\":\"AB14 0TQ\",\"longitude\":-2.097806027," +
                        "\"latitude\":57.14870708},{\"postcode\":\"AB15 6NA\",\"longitude\":-2.2684857520,\"latitude\":57.10155692}]," +
                        "\"distance\":3.781216,\"unit\":\"km\"}"));
    }


    @Test
    public void getDistanceUnauthorizedUserTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(FROM_PARAM_NAME, FROM_POSTCODE)
                        .param(TO_PARAM_NAME, TO_POSTCODE))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getDistanceFromPostcodeIsMissedTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(TO_PARAM_NAME, TO_POSTCODE))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Required [fromPostcode] parameter is missing\"}"));
    }

    @Test
    public void getDistanceToPostcodeIsMissedTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(FROM_PARAM_NAME, FROM_POSTCODE))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Required [toPostcode] parameter is missing\"}"));
    }

    @Test
    public void getDistanceBothPostCodeParamsAreMissedTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Required [fromPostcode] parameter is missing\"}"));
    }

    @Test
    public void getDistanceInvalidPostcodeTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(FROM_PARAM_NAME, FROM_POSTCODE)
                        .param(TO_PARAM_NAME, NON_VALID_POST_CODE))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Invalid post code 'AB156NA'\"}"));
    }

    @Test
    public void postCodeNotFoundTest() throws Exception {
        when(postcodeService.getByCode(FROM_POSTCODE)).thenThrow(new PostcodeNotFoundException("Post code " + FROM_POSTCODE + " not found"));
        mockMvc.perform(
                get(ROOT_PATH)
                        .with(user(ADMIN))
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .param(FROM_PARAM_NAME, FROM_POSTCODE)
                        .param(TO_PARAM_NAME, TO_POSTCODE))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"message\":\"Post code AB14 0TQ not found\"}"));
    }
}
