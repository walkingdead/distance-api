package com.distance.unit.controller;

import com.distance.controller.PostcodeController;
import com.distance.dto.PostcodeDto;
import com.distance.exceptions.PostcodeDuplicateException;
import com.distance.exceptions.PostcodeNotFoundException;
import com.distance.model.Postcode;
import com.distance.service.PostcodeService;
import com.distance.utils.DistanceUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang3.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.hamcrest.CoreMatchers.containsString;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(PostcodeController.class)
public class PostcodeControllerTest {

    private static final String ROOT_PATH = "/postcode";
    private static final String POSTCODE_PATH = ROOT_PATH + "/code/";
    private static final String ADMIN = "admin@gmail.com";
    private static final String USER = "user@gmail.com";

    private static final Long POSTCODE_ID = 1L;
    private static final String POSTCODE_CODE = "AB21 7LZ";

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext context;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private PostcodeService postcodeService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(context)
                .apply(springSecurity())
                .build();
    }

    @Test
    public void getPostcodeByIdUnauthorizedTes() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .requestAttr("id", POSTCODE_ID))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void getPostcodeByIdSuccessTest() throws Exception {
        when(postcodeService.getById(POSTCODE_ID)).thenReturn(new Postcode(POSTCODE_ID, POSTCODE_CODE, 56D, -3D));
        mockMvc.perform(
                get(ROOT_PATH + "/" + POSTCODE_ID)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(ADMIN)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":1,\"postcode\":\"AB21 7LZ\",\"latitude\":56.0,\"longitude\":-3.0}"));
    }

    @Test
    public void getPostcodeByIdNotFoundTest() throws Exception {
        when(postcodeService.getById(POSTCODE_ID))
                .thenThrow(new PostcodeNotFoundException("Post code with id " + POSTCODE_ID + " doesn't exist"));
        mockMvc.perform(
                get(ROOT_PATH + "/" + POSTCODE_ID)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"message\":\"Post code with id 1 doesn't exist\"}"));
    }


    @Test
    public void getPostcodeByCodeSuccessTest() throws Exception {
        when(postcodeService.getByCode(POSTCODE_CODE)).thenReturn(new Postcode(POSTCODE_ID, "AB21 7LQ", 57D, -2D));
        mockMvc.perform(
                get(POSTCODE_PATH + POSTCODE_CODE)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":1,\"postcode\":\"AB21 7LQ\",\"latitude\":57.0,\"longitude\":-2.0}"));
    }

    @Test
    public void getPostcodeByCodeNotFoundTest() throws Exception {
        when(postcodeService.getByCode(POSTCODE_CODE))
                .thenThrow(new PostcodeNotFoundException("Post code with code " + POSTCODE_CODE + " doesn't exist"));
        mockMvc.perform(
                get(POSTCODE_PATH + POSTCODE_CODE)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER)))
                .andExpect(status().isNotFound())
                .andExpect(content().string("{\"message\":\"Post code with code AB21 7LZ doesn't exist\"}"));
    }

    @Test
    public void getAllPostcodePageParameterIsMissedTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER))
                        .param("size", "20"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Required [page] parameter is missing\"}"));
    }

    @Test
    public void getAllPostcodePageSizeParameterIsMissedTest() throws Exception {
        mockMvc.perform(
                get(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER))
                        .param("page", "1"))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Required [size] parameter is missing\"}"));
    }

    @Test
    public void getAllPostcodeSuccessTest() throws Exception {
        List<PostcodeDto> postcodes = Stream.of(
                new Postcode(1L, "AB21 7ND", 55D, -2D),
                new Postcode(2L, "AB21 7NE", 56D, -3D)
        ).map(DistanceUtils::toPostcodeDto).collect(Collectors.toList());
        when(postcodeService.getPostcodes(0, 20))
                .thenReturn(new PageImpl<>(postcodes, new PageRequest(1, 20), 2));
        mockMvc.perform(
                get(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .with(user(USER))
                        .param("page", "1")
                        .param("size", "20"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString("content\":[{\"id\":1,\"postcode\":\"AB21 7ND\"," +
                        "\"latitude\":55.0,\"longitude\":-2.0},{\"id\":2,\"postcode\":\"AB21 7NE\"," +
                        "\"latitude\":56.0,\"longitude\":-3.0}]")));
    }

    @Test
    public void updatePostcodeSuccessTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(1L, "AB21 7ND", 55D, -2D);
        mockMvc.perform(
                put(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(USER))
                        .content(objectMapper.writeValueAsString(postcodeDto)))
                .andExpect(status().isNoContent());
    }

    @Test
    public void savePostcodeSuccessTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(1L, "AB21 7ND", 55D, -2D);
        Postcode postcode = new Postcode(1L, "AB21 7ND", 55D, -2D);
        when(postcodeService.createPostcode(postcodeDto)).thenReturn(postcode);
        mockMvc.perform(
                post(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(USER))
                        .content(objectMapper.writeValueAsString(postcodeDto)))
                .andExpect(status().isOk())
                .andExpect(content().string("{\"id\":1}"));
    }

    @Test
    public void savePostcodeDuplicateTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(1L, "AB21 7ND", 55D, -2D);
        when(postcodeService.createPostcode(postcodeDto))
                .thenThrow(new PostcodeDuplicateException(String.format("Postcode %s already exists", postcodeDto.getPostcode())));
        mockMvc.perform(
                post(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(USER))
                        .content(objectMapper.writeValueAsString(postcodeDto)))
                .andExpect(status().isConflict())
                .andExpect(content().string("{\"message\":\"Postcode AB21 7ND already exists\"}"));
    }

    @Test
    public void updatePostcodeMissingIdParameterTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(null, "AB21 7ND", 55D, -2D);
        mockMvc.perform(
                put(ROOT_PATH)
                        .accept(MediaType.parseMediaType(APPLICATION_JSON_VALUE))
                        .contentType(MediaType.APPLICATION_JSON)
                        .with(user(USER))
                        .content(objectMapper.writeValueAsString(postcodeDto)))
                .andExpect(status().isBadRequest())
                .andExpect(content().string("{\"message\":\"Missing required 'id' parameter\"}"));
    }

    @Test
    public void updatePostcodeNullLatitudeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7ND", null, -2D);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Latitude is required"}, put(ROOT_PATH));
    }

    @Test
    public void updatePostcodeNullLongitudeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7ND", 57.0568, null);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Longitude is required"}, put(ROOT_PATH));
    }

    @Test
    public void updatePostcodeNonValidPostcodeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7865ND", 57.0568, -2.0);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Invalid post code 'AB2 7865ND'"}, put(ROOT_PATH));
    }

    @Test
    public void createPostcodeNullLatitudeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7ND", null, -2D);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Latitude is required"}, post(ROOT_PATH));
    }

    @Test
    public void createPostcodeNullLongitudeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7ND", 57.0568, null);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Longitude is required"}, post(ROOT_PATH));
    }

    @Test
    public void createPostcodeNonValidPostcodeTest() throws Exception {
        PostcodeDto postcodeDto = new PostcodeDto(2L, "AB2 7865ND", 57.0568, -2.0);
        verifyResults(postcodeDto, status().isBadRequest(), new String []{"Invalid post code 'AB2 7865ND'"}, post(ROOT_PATH));
    }

    private void verifyResults(PostcodeDto postcodeDto, ResultMatcher status, String[] errorMessages,
                               MockHttpServletRequestBuilder builder) throws Exception {
        MvcResult result = this.mockMvc.perform(builder
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON)
                .with(user(USER))
                .content(objectMapper.writeValueAsString(postcodeDto)))
                .andExpect(status)
                .andReturn();
        String rawResponse = result.getResponse().getContentAsString();
        List<String> errors = new ArrayList<>();
        if (StringUtils.isNotBlank(rawResponse))
            errors = Arrays.asList(rawResponse.substring(rawResponse.indexOf('[') + 1, rawResponse.lastIndexOf(']')).split(", "));
        assertThat(errors, hasItems(errorMessages));
    }
}
