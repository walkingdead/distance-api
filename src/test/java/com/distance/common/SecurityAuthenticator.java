package com.distance.common;


import com.distance.security.request.JwtAuthenticationRequest;
import com.distance.security.response.JwtAuthenticationResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

@Component
public class SecurityAuthenticator {

    @Autowired
    private ObjectMapper objectMapper;

    public HttpHeaders getAuthHeader(RestTemplate restTemplate, String port, String name, String password) throws JsonProcessingException {
        MultiValueMap<String, String> securityHeaders = new LinkedMultiValueMap<>();
        securityHeaders.add("Content-Type", "application/json");

        JwtAuthenticationRequest authenticationRequest = new JwtAuthenticationRequest(name, password);

        HttpEntity<String> credentials = new HttpEntity<>(objectMapper.writeValueAsString(authenticationRequest), securityHeaders);
        HttpEntity<JwtAuthenticationResponse> authResponse = restTemplate.exchange("http://localhost:{port}/auth",
                HttpMethod.POST, credentials, JwtAuthenticationResponse.class, port);
        JwtAuthenticationResponse authenticationResponse = authResponse.getBody();

        HttpHeaders headers = new HttpHeaders();
        headers.add("Authorization", authenticationResponse.getToken());

        return headers;
    }
}
