package com.distance.common;


import com.distance.dto.PostcodeDto;

import java.util.List;

public class PostcodePageable {

    private List<PostcodeDto> content;
    private Long total;

    public PostcodePageable() {
    }

    public List<PostcodeDto> getContent() {
        return content;
    }

    public void setContent(List<PostcodeDto> content) {
        this.content = content;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
