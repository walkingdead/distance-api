package com.distance.exceptions;


public class PostcodeNotFoundException extends RuntimeException {
    public PostcodeNotFoundException() {
        super();
    }

    public PostcodeNotFoundException(String message) {
        super(message);
    }

    public PostcodeNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
