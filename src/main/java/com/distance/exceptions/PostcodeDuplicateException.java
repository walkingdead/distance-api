package com.distance.exceptions;


public class PostcodeDuplicateException extends RuntimeException {
    public PostcodeDuplicateException() {
        super();
    }

    public PostcodeDuplicateException(String message) {
        super(message);
    }

    public PostcodeDuplicateException(String message, Throwable cause) {
        super(message, cause);
    }
}
