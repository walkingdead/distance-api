package com.distance.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

import java.util.List;

public class DistanceDto {

    private List<LocationDto> locations;
    private Double distance;
    private String unit = "km";

    public DistanceDto() {
    }

    public DistanceDto(List<LocationDto> locations, Double distance) {
        this.locations = locations;
        this.distance = distance;
    }
    
    public List<LocationDto> getLocations() {
        return locations;
    }

    public void setLocations(List<LocationDto> locations) {
        this.locations = locations;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof DistanceDto)) {
            return false;
        }

        DistanceDto other = (DistanceDto) o;
        return new EqualsBuilder()
                .append(locations, other.getLocations())
                .append(distance, other.getDistance())
                .append(unit, other.getUnit())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(locations)
                .append(distance)
                .append(unit)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("locations", locations)
                .append("distance", distance)
                .append("unit", unit)
                .toString();
    }
}
