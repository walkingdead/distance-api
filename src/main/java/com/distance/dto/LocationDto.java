package com.distance.dto;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

public class LocationDto {

    private String postcode;
    private Double longitude;
    private Double latitude;

    public LocationDto(String postcode, Double longitude, Double latitude) {
        this.postcode = postcode;
        this.longitude = longitude;
        this.latitude = latitude;
    }

    public LocationDto() {
    }

    public String getPostcode() {
        return postcode;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }

        if (!(o instanceof LocationDto)) {
            return false;
        }

        LocationDto other = (LocationDto) o;
        return new EqualsBuilder()
                .append(postcode, other.getPostcode())
                .append(longitude, other.getLongitude())
                .append(latitude, other.getLatitude())
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder()
                .append(postcode)
                .append(longitude)
                .append(latitude)
                .toHashCode();
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("postcode", postcode)
                .append("longitude", longitude)
                .append("latitude", latitude)
                .toString();
    }
}
