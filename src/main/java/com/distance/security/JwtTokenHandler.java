package com.distance.security;


import com.distance.security.common.JwtSettings;
import com.distance.utils.DateTimeUtils;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import org.springframework.security.core.userdetails.UserDetails;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Base64;
import java.util.Date;
import java.util.UUID;


public class JwtTokenHandler {

    private final String secret;
    private final JwtSettings jwtSettings;

    public JwtTokenHandler(JwtSettings jwtSettings) {
        this.secret = Base64.getEncoder().encodeToString(jwtSettings.getSecret().getBytes());
        this.jwtSettings = jwtSettings;
    }

    public String parseUserNameFromToken(String authToken) {
        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(authToken)
                .getBody()
                .getSubject();
    }

    public String createTokenForUser(UserDetails user) {
        LocalDateTime now = LocalDateTime.now();
        Date expiration = DateTimeUtils.asDateTime(now.plus(jwtSettings.getExpiration(), ChronoUnit.MINUTES));
        return Jwts.builder()
                .setId(UUID.randomUUID().toString())
                .setSubject(user.getUsername())
                .setIssuedAt(DateTimeUtils.asDateTime(now))
                .setExpiration(expiration)
                .signWith(SignatureAlgorithm.HS512, secret)
                .compact();
    }
}
