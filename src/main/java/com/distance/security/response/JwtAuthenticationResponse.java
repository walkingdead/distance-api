package com.distance.security.response;

import java.io.Serializable;

public class JwtAuthenticationResponse implements Serializable {

    private String token;

    public JwtAuthenticationResponse(String token) {
        this.token = token;
    }

    public JwtAuthenticationResponse() {
    }

    public String getToken() {
        return this.token;
    }
}
