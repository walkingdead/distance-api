package com.distance.controller;

import com.distance.dto.DistanceDto;
import com.distance.dto.LocationDto;
import com.distance.model.Postcode;
import com.distance.service.DistanceService;
import com.distance.service.PostcodeService;
import com.distance.utils.DistanceUtils;
import com.distance.validator.ValidPostcode;
import org.hibernate.validator.constraints.NotEmpty;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

@RestController
@Validated
public class DistanceController {

    private static final Logger LOGGER = LoggerFactory.getLogger(DistanceController.class);

    private final PostcodeService postcodeService;
    private final DistanceService distanceService;

    @Autowired
    public DistanceController(PostcodeService postcodeService, DistanceService distanceService) {
        this.postcodeService = postcodeService;
        this.distanceService = distanceService;
    }

    @GetMapping(value = "/distance")
    public ResponseEntity<DistanceDto> getDistance(@RequestParam @NotEmpty @ValidPostcode String fromPostcode,
                                                   @RequestParam @NotEmpty @ValidPostcode String toPostcode) {
        LOGGER.trace("Calculating distance between postcode '{}' and '{}'", fromPostcode, toPostcode);
        Postcode fromPostcodeData = postcodeService.getByCode(fromPostcode);
        Postcode toPostcodeData = postcodeService.getByCode(toPostcode);

        double distance = distanceService.calculateDistance(fromPostcodeData, toPostcodeData);
        LOGGER.trace("Calculated distance is: '{}' km", distance);
        List<LocationDto> locations = Stream.of(fromPostcodeData, toPostcodeData)
                .map(DistanceUtils::toLocationDto)
                .collect(toList());

        DistanceDto distanceDto = new DistanceDto(locations, distance);
        return ResponseEntity.ok(distanceDto);
    }
}