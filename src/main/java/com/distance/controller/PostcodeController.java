package com.distance.controller;

import com.distance.common.ErrorEntity;
import com.distance.dto.PostcodeDto;
import com.distance.exceptions.PostcodeNotFoundException;
import com.distance.model.Postcode;
import com.distance.service.PostcodeService;
import com.distance.utils.DistanceUtils;
import com.google.common.collect.ImmutableMap;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Map;

@RestController
@RequestMapping("/postcode")
@Validated
public class PostcodeController {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostcodeController.class);

    private final PostcodeService postcodeService;

    @Autowired
    public PostcodeController(PostcodeService postcodeService) {
        this.postcodeService = postcodeService;
    }

    @GetMapping(value = "/{id}")
    public ResponseEntity<PostcodeDto> getPostcodeById(@PathVariable Long id) throws PostcodeNotFoundException {
        LOGGER.info("Get postcode by id '{}'", id);
        return ResponseEntity.ok(DistanceUtils.toPostcodeDto(postcodeService.getById(id)));
    }

    @GetMapping(value = "/code/{code}")
    public ResponseEntity<PostcodeDto> getPostcodeByCode(@PathVariable String code) throws PostcodeNotFoundException {
        LOGGER.info("Get postcode by code '{}'", code);
        return ResponseEntity.ok(DistanceUtils.toPostcodeDto(postcodeService.getByCode(code)));
    }

    @GetMapping("")
    public ResponseEntity<Page<PostcodeDto>> getPostcodes(@RequestParam @Min(value = 1, message = "Min page value is 1") Integer page,
                                                          @RequestParam Integer size) {
        LOGGER.info("Get postcodes with page '{}' and  size '{}'", page, size);
        return ResponseEntity.ok(postcodeService.getPostcodes(page - 1, size));
    }

    @PutMapping(value = "")
    public ResponseEntity updatePostcode(@RequestBody  @Valid PostcodeDto postcodeDto) {
        LOGGER.info("Update postcode '{}'", postcodeDto);
        if(postcodeDto.getId() == null){
            return ResponseEntity.badRequest().body(new ErrorEntity("Missing required 'id' parameter"));
        }
        postcodeService.updatePostcode(postcodeDto);
        return ResponseEntity.noContent().build();
    }

    @PostMapping(value = "")
    public ResponseEntity createPostcode(@RequestBody @Valid PostcodeDto postcodeDto) {
        LOGGER.info("Create postcode '{}'", postcodeDto);
        Postcode postcode = postcodeService.createPostcode(postcodeDto);
        Map<String, Long> newId = ImmutableMap.<String, Long>builder()
                .put("id", postcode.getId())
                .build();
        return ResponseEntity.ok(newId);
    }
}
