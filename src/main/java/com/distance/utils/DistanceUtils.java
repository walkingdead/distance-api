package com.distance.utils;


import com.distance.dto.LocationDto;
import com.distance.dto.PostcodeDto;
import com.distance.model.Postcode;

public class DistanceUtils {

    private DistanceUtils() {
        throw new IllegalArgumentException("Can't be instantiated");
    }

    public static LocationDto toLocationDto(Postcode postcode) {
        return new LocationDto(
                postcode.getPostcode(),
                postcode.getLongitude(),
                postcode.getLatitude());
    }

    public static PostcodeDto toPostcodeDto(Postcode postcode) {
        return new PostcodeDto(postcode.getId(),
                postcode.getPostcode(),
                postcode.getLatitude(),
                postcode.getLongitude());
    }

    public static Postcode toPostcode(PostcodeDto postcodeDto) {
        return new Postcode(postcodeDto.getId(),
                postcodeDto.getPostcode(),
                postcodeDto.getLongitude(),
                postcodeDto.getLatitude());
    }
}
