package com.distance.utils;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;


public class DateTimeUtils {

    private DateTimeUtils() {
        throw new IllegalArgumentException("Can't be instantiated");
    }

    public static Date asDateTime(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
}
