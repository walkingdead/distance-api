package com.distance.service;

import com.distance.dto.PostcodeDto;
import com.distance.exceptions.PostcodeDuplicateException;
import com.distance.exceptions.PostcodeNotFoundException;
import com.distance.model.Postcode;
import com.distance.repository.PostcodeRepository;
import com.distance.utils.DistanceUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;

import java.util.function.Supplier;

@Service
public class PostcodeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostcodeService.class);

    @Autowired
    private PostcodeRepository postcodeRepository;

    public Postcode getById(Long id) {
        Postcode postcode = postcodeRepository.findOne(id);
        checkPostCode(postcode, () -> String.format("Postcode id [%d] not found", id));
        return postcode;
    }

    public Postcode getByCode(String code) {
        Postcode postcode = postcodeRepository.findByPostcode(code);
        checkPostCode(postcode, () -> String.format("Postcode [%s] not found", code));
        return postcode;
    }

    public Page<PostcodeDto> getPostcodes(Integer page, Integer size) {
        Pageable pageable = new PageRequest(page, size);
        return postcodeRepository.findAll(pageable).map(DistanceUtils::toPostcodeDto);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Postcode updatePostcode(PostcodeDto postcodeDto) {
        Postcode postcodeSaved = getById(postcodeDto.getId());
        LOGGER.debug("Updating saved post code '{}' with new one '{}'", postcodeSaved, postcodeDto);
        postcodeSaved.setLatitude(postcodeDto.getLatitude());
        postcodeSaved.setLongitude(postcodeDto.getLongitude());
        postcodeSaved.setPostcode(postcodeDto.getPostcode());
        return postcodeRepository.save(postcodeSaved);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    public Postcode createPostcode(PostcodeDto postcodeDto) {
        LOGGER.debug("Create new post code '{}' ", postcodeDto);
        Postcode postcode = this.postcodeRepository.findByPostcode(postcodeDto.getPostcode());
        if (postcode != null) {
            throw new PostcodeDuplicateException(String.format("Postcode %s already exists", postcodeDto.getPostcode()));
        }
        return postcodeRepository.save(DistanceUtils.toPostcode(postcodeDto));
    }

    private void checkPostCode(Postcode postcode, Supplier<String> messageCreator) {
        if (postcode == null) {
            String message = messageCreator.get();
            LOGGER.error(message);
            throw new PostcodeNotFoundException(message);
        }
    }
}
