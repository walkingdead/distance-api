package com.distance.service;

import com.distance.model.Postcode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class DistanceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DistanceService.class);

    private static final int EARTH_RADIUS = 6371; // Earth radius in kilometers

    public double calculateDistance(Postcode fromPostcode, Postcode toPostcode) {
        double fromLongitudeRadians = Math.toRadians(fromPostcode.getLongitude());
        double toLongitudeRadians = Math.toRadians(toPostcode.getLongitude());

        double fromLatitudeRadians = Math.toRadians(fromPostcode.getLatitude());
        double toLatitudeRadians = Math.toRadians(toPostcode.getLatitude());

        double haversine = calculateHaversine(fromLatitudeRadians, toLatitudeRadians)
                + Math.cos(fromLongitudeRadians)
                * Math.cos(toLongitudeRadians) * calculateHaversine(fromLongitudeRadians, toLongitudeRadians);

        double c = 2 * Math.atan2(Math.sqrt(haversine), Math.sqrt(1 - haversine));
        return EARTH_RADIUS * c;
    }

    private double calculateHaversine(double fromLatitudeRadians, double toLatitudeRadians) {
        LOGGER.info("Calculate haversine latitude radians '{}' and '{}'", fromLatitudeRadians, toLatitudeRadians);
        return square(Math.sin((fromLatitudeRadians - toLatitudeRadians) / 2.0));
    }

    private double square(double x) {
        return x * x;
    }
}
