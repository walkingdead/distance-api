package com.distance.validator;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Pattern;

public class PostcodeValidator implements ConstraintValidator<ValidPostcode, String> {

    private static final Logger LOGGER = LoggerFactory.getLogger(PostcodeValidator.class);

    private static final Pattern POSTCODE_PATTERN = Pattern.compile("[A-Z]{1,2}\\d{1,2}[A-Z]?\\s\\d[A-Z]{2}");

    @Override
    public void initialize(ValidPostcode validPostcode) {

    }

    @Override
    public boolean isValid(String postcode, ConstraintValidatorContext constraintValidatorContext) {

        if (StringUtils.isNotBlank(postcode) && !POSTCODE_PATTERN.matcher(postcode).matches()) {
            LOGGER.error("Postcode '{}' format is invalid", postcode);
            constraintValidatorContext.disableDefaultConstraintViolation();
            constraintValidatorContext
                    .buildConstraintViolationWithTemplate(String.format("Invalid post code '%s'", postcode))
                    .addConstraintViolation();
            return false;
        }
        return true;
    }
}
