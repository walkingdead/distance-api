package com.distance;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DistanceApiApplication {

    private static final Logger logger = LoggerFactory.getLogger(DistanceApiApplication.class);

    public static void main(String[] args) {
        SpringApplication.run(DistanceApiApplication.class);
        logger.info("Application DistanceApiApplication has started");
    }
}
